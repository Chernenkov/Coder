import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class App {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

//        ArrayList<Integer> g0 = new ArrayList<>(Arrays.asList(1, 1, 0, 1));
        ArrayList<Integer> g0 = new ArrayList<>(Arrays.asList(1, 1, 1, 0, 1));
//        // l == k
//        run(g0, 4);
        run2(g0, 4);

//        Histogram.draw();
//        ArrayList<Integer> g1 = new ArrayList<>(Arrays.asList(1, 1, 0, 1));
//        // l < k
//        run(g1, 8);

//        ArrayList<Integer> g2 = new ArrayList<>(Arrays.asList(1, 1, 0, 1));
//        // l > k
//        run(g2, 1);
    }

    private static void run(ArrayList<Integer> g, int k) {
        int r = getDegree(g);
        ArrayList<ArrayList<Integer>> codewords = getCodewords(k, r, g);

        int d = getD(codewords);
        int n = k + r;

        double[] p = new double[100];
        double[] pe = new double[100];
        double[] peLimit = new double[100];

        for (int i = 0; i < 100; i++) {
//            p[i] = 0.1;//(double) i / 100;
            p[i] = (double) i / 100;
            //
//            pe[i] = pe(n, d, 0.1, codewords);
//            peLimit[i] = peLimit(n, d, 0.1);
            //
//            pe[i] = pe(n, d, 0.2, codewords);
//            peLimit[i] = peLimit(n, d, 0.2);
            //
//            pe[i] = pe(n, d, 0.3, codewords);
//            peLimit[i] = peLimit(n, d, 0.3);
            //
            // main task
            pe[i] = pe(n, d, p[i], codewords);
            peLimit[i] = peLimit(n, d, p[i]);
        }

        Histogram.draw(p, pe, "Pe" + getShow(g, "(g)") + " k = " + k);
        Histogram.draw(p, peLimit, "P_error_Limit" + getShow(g, "(g)") + " k = " + k);
    }

    private static void run2(ArrayList<Integer> g, int k) {
        int r = getDegree(g);
        ArrayList<ArrayList<Integer>> codewords = getCodewords(k, r, g);
        int d = getD(codewords);
        int n = k + r;
        double[] p = new double[14];
        double[] pe = new double[14];
        double[] peLimit = new double[14];
        double[] sizes = new double[14];
//        System.out.println("1");
        for (int i = 0; i < 14; i++) {
            System.out.println("1");
            p[i] = 0.3;
            pe[i] = pe(n, d, p[i], codewords);
            peLimit[i] = peLimit(n, d, p[i]);
            sizes[i] = k;
            k++;
            codewords = getCodewords(k, r, g);
            d = getD(codewords);
            n = k + r;
        }
        System.out.println("2");
        Histogram.drawExtra(sizes, pe, "Pe" + " " + "k = " + k); // getShow(g, "(g)")
        Histogram.drawExtra(sizes, peLimit, "P_error_Limit" + " " + " k = " + k); // getShow(g, "(g)")
    }

    private static ArrayList<Integer> encode(ArrayList<Integer> m, int r, ArrayList<Integer> g) {
        ArrayList<Integer> c = divide(shiftLeft(m, r), g);
        return add(shiftLeft(m, r), c);
    }

     // upper border error value
    private static double peLimit(int n, int d, double p) {
        double result = 0;
        for (int i = 0; i < d; i++) {
            result += (combinations(i, n) * Math.pow(p, i)) * Math.pow((1 - p), (n - i));
        }
        result = 1 - result;
        return result;
    }

    // exact error value
    private static double pe(int n, int d, double p, ArrayList<ArrayList<Integer>> codewords) {
        double result = 0;
        for (int i = d; i <= n; i++) {
            int A = getCodewordsCount(codewords, i);
            result += (A * Math.pow(p, i)) * Math.pow((1 - p), (n - i));
        }
        return result;
    }

    private static int getCodewordsCount(ArrayList<ArrayList<Integer>> codewords, int weight) {
        int result = 0;
        for (ArrayList<Integer> codeword: codewords) {
            if (getWeight(codeword) == weight) {
                result += 1;
            }
        }
        return result;
    }

    private static ArrayList<ArrayList<Integer>> getCodewords(int k, int r, ArrayList<Integer> g) {
        ArrayList<ArrayList<Integer>> words = new ArrayList<>();
        ArrayList<ArrayList<Integer>> result = new ArrayList<>();

        ArrayList<Integer> m = new ArrayList<>();
        for (int i = 0; i < k; i++) { m.add(0); }

        for (int i = 0; i < Math.pow(2, k); i++) {
            words.add(new ArrayList<>(m));
            for (int j = 0; j < m.size(); j++) {
                if (m.get(j) == 0) {
                    m.set(j, 1);
                    break;
                }
                m.set(j, 0);
            }
        }

        for (int i = 0; i < Math.pow(2, k); i++) {
            result.add(encode(words.get(i), r, g));
        }

        return result;
    }

    private static int getD(ArrayList<ArrayList<Integer>> codewords) {
        int d = codewords.get(0).size();
        for (ArrayList<Integer> codeword: codewords) {
            int weight = getWeight(codeword);
            if (weight != 0 && weight < d) {
                d = weight;
            }
        }
        return d;
    }

    private static int getWeight(ArrayList<Integer> codeword) {
        int count = 0;
        for (int i = 0; i < codeword.size(); i++) {
            if (codeword.get(i) == 1) {
                count += 1;
            }
        }
        return count;
    }

    private static ArrayList<Integer> divide(ArrayList<Integer> a, ArrayList<Integer> b) {
        ArrayList<Integer> rest = new ArrayList<>(a);
        int bDegree = getDegree(b);
        int aDegree;
        while ((aDegree = getDegree(rest)) >= bDegree) {
            int difference = aDegree - bDegree;
            if (difference > 0) {
                rest = add(rest, shiftLeft(b, difference));
            } else {
                rest = add(rest, b);
            }
        }
        popZeros(rest);
        return rest;
    }


    private static ArrayList<Integer> add(ArrayList<Integer> a, ArrayList<Integer> b) {
        ArrayList<Integer> result = new ArrayList<>(a);
        for (int i = 0; i < (a.size() > b.size() ? b.size() : a.size()); i++) {
            result.set(i, (a.get(i) + b.get(i)) % 2);
        }
        return result;
    }

    private static int getDegree(ArrayList<Integer> polynom) {
        for(int i = polynom.size() - 1; i >= 0; i--) {
            if (polynom.get(i) != 0) { return i; };
        }
        return -1;
    }

    private static ArrayList<Integer> shiftLeft(ArrayList<Integer> polynom, int positions) {
        ArrayList<Integer> result = new ArrayList<>(polynom);
        for(int i = 0; i < positions; i++) {
            result.add(0, 0);
        }
        return result;
    }

    private static void popZeros(ArrayList<Integer> polynom) {
        for (int i = polynom.size() - 1; i >= 0; i--) {
            if (polynom.get(i) == 1) return;
            polynom.remove(polynom.size() - 1);
        }
    }

    private static void show(ArrayList polynom, String id) {
        System.out.print(id + ": ");
        for (int i = polynom.size() - 1; i >= 0; i--) {
            System.out.print(polynom.get(i) + " ");
        }
        System.out.print("\n");
    }

    private static String getShow(ArrayList polynom, String id) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(id + ": ");
        for (int i = polynom.size() - 1; i >= 0; i--) {
            stringBuilder.append(polynom.get(i) + " ");
        }
        stringBuilder.append("\n");
        return stringBuilder.toString();
    }

    // Combinations
    private static int factorial(int a) {
        if (a == 0 || a == 1) return 1;

        int result = 1;

        for (int i = 2; i <= a; i++) {
            result *= i;
        }

        return result;
    }

    private static int combinations(int k, int n) {
        if (factorial(n-k)*factorial(k) == 0) return 0;
        return factorial(n) / (factorial(n - k) * factorial(k));
    }
}