import org.knowm.xchart.BitmapEncoder;
import org.knowm.xchart.QuickChart;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.*;

import java.io.IOException;

class Histogram {

    static void draw(double[] x, double[] y, String name) {
        XYChart chart = QuickChart.getChart(name, "p", name, name, x, y);
        new SwingWrapper<>(chart).displayChart();

        try {
            BitmapEncoder.saveBitmap(chart, "./" + name, BitmapEncoder.BitmapFormat.PNG);
        } catch (IOException e) {
            System.out.println("error");
        }
    }

    static void drawExtra(double[] x, double[] y, String name) {
        XYChart chart = QuickChart.getChart(name, "l", name, name, x, y);
        new SwingWrapper<>(chart).displayChart();

        try {
            BitmapEncoder.saveBitmap(chart, "./" + name, BitmapEncoder.BitmapFormat.PNG);
        } catch (IOException e) {
            System.out.println("error");
        }
    }

}